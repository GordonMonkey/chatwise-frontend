# base image
FROM node:18.19.0-alpine as builder

ARG NODE_ENV
ARG PORT
ARG VUE_APP_API_URL

ENV NODE_ENV $NODE_ENV
ENV PORT $PORT
ENV VUE_APP_API_URL $VUE_APP_API_URL

WORKDIR /app
COPY . /app
COPY package.json ./

RUN npm install
RUN npm run build

# Copy the resulting code to the nginx container
FROM nginx:alpine

ARG NODE_ENV
ARG PORT
ARG VUE_APP_API_URL

ENV NODE_ENV $NODE_ENV
ENV PORT $PORT
ENV VUE_APP_API_URL $VUE_APP_API_URL

COPY .docker/nginx/nginx.conf /etc/nginx/nginx.conf
ADD  .docker/env/${NODE_ENV}/conf.d /etc/nginx/conf.d

COPY --from=builder /app/dist /usr/share/nginx/html

EXPOSE ${PORT}

WORKDIR /usr/share/nginx/html

CMD ["nginx"]
